export interface ICoach {
    coachName: string;
    email?: string;
    phoneNumber?: string;
    courseId?: number;
    createdAt:string;           
    updatedAt:string;          
    deletedAt?:string;
}