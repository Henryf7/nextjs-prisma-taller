export interface Icourse {
    courseName: string;
    description: string;
    startDate: string;
    endDate: string;
}