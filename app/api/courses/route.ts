import prisma from "@/app/lib/prisma";
import { Icourse } from "@/utils/interfaces/ICourse";
import { NextResponse } from "next/server";

export async function GET(request: Request, context: any) {
    const courses = await prisma.course.findMany();

    return NextResponse.json({
        status: 200,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            courses,
        },
    });
}

export async function POST(request: Request, context: any) {
    const { courseName, description, endDate, startDate } : Icourse = await request.json();

    const coach = await prisma.course.create({
        data: {
            courseName,
            description,
            endDate,
            startDate,
        },
    });

    return NextResponse.json({
        status: 201,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            coach,
        },
    });
}