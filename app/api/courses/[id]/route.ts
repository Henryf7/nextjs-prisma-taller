import prisma from "@/app/lib/prisma";
import { Icourse } from "@/utils/interfaces/ICourse";
import { NextResponse } from "next/server";

type Context = {
    params: {
        id: number;
    };
};

export async function GET(request: Request, context: Context) {
    const course = await prisma.course.findUnique({
        where: {
          id: context.params.id,
        },
      })

    return NextResponse.json({ data: course, status: 200 });
}

export async function PATCH(request: Request, context: Context) {
    const { courseName, description, endDate, startDate } : Icourse = await request.json();

    const updateCourse = await prisma.course.update({
        where: {
          id: context.params.id,
        },
        data: {
          courseName,
          description,
          startDate,
          endDate,
        },
      })

    return NextResponse.json({ data: updateCourse, status: 200 });
}

export async function DELETE(request: Request, context: Context) {
    const deleteCourse = await prisma.coach.delete({
        where: {
          id: context.params.id,
        },
    })

    return NextResponse.json({ status: 200 });
}