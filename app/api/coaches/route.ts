import prisma from "@/app/lib/prisma";
import { ICoach } from "@/utils/interfaces/ICoach";
import { NextResponse } from "next/server";

export async function GET(request: Request, context: any) {
    const coaches = await prisma.coach.findMany();

    return NextResponse.json({
        status: 200,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            coaches,
        },
    });
}

export async function POST(request: Request, context: any) {
    const { coachName, courseId, email, phoneNumber } : ICoach = await request.json();

    const coach = await prisma.coach.create({
        data: {
            coachName,
            email,
            phoneNumber,
            courseId,
        },
    });

    return NextResponse.json({
        status: 201,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            coach,
        },
    });
}
