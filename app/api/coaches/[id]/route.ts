import prisma from "@/app/lib/prisma";
import { ICoach } from "@/utils/interfaces/ICoach";
import { NextResponse } from "next/server";

type Context = {
    params: {
        id: number;
    };
};

export async function GET(request: Request, context: Context) {
    const coach = await prisma.coach.findUnique({
        where: {
          id: context.params.id,
        },
      })

    return NextResponse.json({ data: coach, status: 200 });
}

export async function PATCH(request: Request, context: Context) {
    const { coachName, courseId, email, phoneNumber } : ICoach = await request.json();

    const updateCoach = await prisma.coach.update({
        where: {
          id: context.params.id,
        },
        data: {
          coachName,
          courseId,
          email,
          phoneNumber,
        },
      })

    return NextResponse.json({ data: updateCoach, status: 200 });
}

export async function DELETE(request: Request, context: Context) {
    const deleteCoach = await prisma.coach.delete({
        where: {
          id: context.params.id,
        },
    })

    return NextResponse.json({ status: 200 });
}